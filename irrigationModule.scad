module sockethole(){
    rotate([135,0,0]) difference(){
        cube([19,12,10]);
        translate([-1,-1,-1])cube([5,4,12]);
        translate([10,-1,-1]) cube([10,4,12]);
    }
}

module pumpHousing(xAxe,yAxe,zAxe,xtrans,rotY){
    rotate([0,rotY,0]) difference(){
        union(){
            cube([xAxe,yAxe,zAxe]);
            translate([0,35,-2]) cube([xAxe,7,2]);
        }
        translate([-1+xtrans,1.5,20]) cube([xAxe+2,yAxe-3,zAxe-22]);
        translate([-1+xtrans,19.5,2]) cube([xAxe+2,yAxe-21,30]);
        translate([-1,20,0]) rotate([135,0,0]) cube([xAxe+2,30,20]);
        translate([-1+xtrans,33.7,16.3]) rotate([135,0,0]) cube([xAxe+2,25.5,20]);
        translate([45,10,35]) rotate([90,0,0]) cylinder(20,5,5,$fn=100);
        translate([45,38.5,-3]) cylinder(20,3.5,3.5,$fn=100);
        translate([35,20,10]) sockethole();
    }
}
module pumpHousingMiddle(xAxe,yAxe,zAxe){
    translate([0,0,90]) 
    pumpHousing(xAxe,yAxe,zAxe,0,90);
}
module pumpHousingRight(xAxe,yAxe,zAxe){
    translate([0,0,90]) difference(){
        pumpHousing(xAxe,yAxe,zAxe,-3,90);
        translate([70,38.5,-92]) 
        rotate([0,0,0]) cylinder(20,3.5,3.5,$fn=100);
    translate([95,10,-45]) rotate([0,90,0]) cylinder(20,5,5,$fn=100);
    }
}
module pumpHousingLeft(xAxe,yAxe,zAxe){
    difference(){
    pumpHousing(xAxe,yAxe,zAxe,3,-90);
    translate([-45,38.5,-10]) 
    cylinder(20,3.5,3.5,$fn=100);
    translate([-101,30,15]) rotate([0,90,0]) cylinder(4,10.25,10.25,$fn=100);
    translate([-101,53,15]) rotate([0,90,0]) cylinder(4,4,4,$fn=100);
    }
}

module bottleHolder(){
    translate([0,0,300]) union(){
        difference(){
            translate([0,0,-300]) cylinder(300, 50,50,$fn=500);
            translate([0,0,-301]) cylinder(302, 48,48,$fn=500);
        }
        difference() {
            cylinder(30,50,10,$fn=500);
            translate([0,0,-1])  cylinder(26,49.7,5,$fn=500);
            translate([0,0,24.5])  cylinder(6,5,5,$fn=500);
        }
    }
}
module bottleHolderPlate(xAxe,yAxe,zAxe){
    translate([0+xAxe,1+yAxe,0+zAxe]) difference(){
        union(){
            cube([40,3,200], center = true);
            translate([0,2.5,0]) cube([7,2,200], center = true);
        }
    for(i=[-90:90:90]){
        translate([0,0,i]) rotate([90,0,0])
        cylinder(20,3.5,3.5,$fn=100,center = true);
        }
    }
}
module bottleHoldersimple(hight,rad1,rad2,spalt){
    difference(){
        hull(){
        cylinder(hight+4,rad1+1.5,rad2+2,$fn=500);
        translate([0,rad1*1,hight/2+2]) cube([40,2,hight+4], center = true);
        }
        translate([0,hight+5,hight/2+40]) cube([20,60,hight/2], center = true);
        translate([0,0,-1])  cylinder(hight,rad1,rad2,$fn=500);
        translate([0,0,72])  cylinder(10,rad2,rad2,$fn=500);
        if(spalt>1){
            translate([0,-53,40]) cube([5,50,82], center = true);
        }
    }
}
module bottleHolder2(hight,rad1,rad2){
    bottleHolderPlate(0,hight+5,-21);
    bottleHoldersimple(hight,rad1,rad2,2);
}
module bottleHolder1(hight,rad1,rad2){
    bottleHolderPlate(0,rad1*1.02,-21);
    bottleHoldersimple(hight,rad1,rad2,1);
}