use <./roundedCube.scad>
module plantPod(length, width, height, rounding)
{
    difference()
    {
        roundedCube([ length, width, height ], false, rounding, "zmin");
        translate([ 2, 2, 2 ]) roundedCube([ length - 4, width - 4, height ], false, rounding, "zmin");
    }
}

module plantLifter(length, width, height, rounding)
{
    difference()
    {
        plantPod(length = length, width = width, height = height, rounding = rounding);
        translate([ length + 2, 0, 0 ]) rotate([ 0, atan(height / length) - 90, 0 ])
            cube([ length, width, 2 + sqrt(height ^ 2 + length ^ 2) ]);

        for (i = [10:10:width - 10])
        {
            for (j = [10:10:height - 10])
            {
                translate([ -1, i, j ]) rotate([ 0, 90, 0 ]) cylinder(h = 4, r = 4);
            }
        }
    }
}

plantPod(95, 110, 130, 4);
// translate([ 98, 0, 0 ]) plantLifter(90, 105, 128, 4);
